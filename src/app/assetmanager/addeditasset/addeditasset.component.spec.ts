import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditassetComponent } from './addeditasset.component';

describe('AddeditassetComponent', () => {
  let component: AddeditassetComponent;
  let fixture: ComponentFixture<AddeditassetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditassetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditassetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
