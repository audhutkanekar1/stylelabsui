import { Component, OnInit } from '@angular/core';
import { AssetService } from '../services/asset.service';
import { MatDialogRef } from '@angular/material';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-addeditasset',
  templateUrl: './addeditasset.component.html',
  styleUrls: ['./addeditasset.component.scss']
})
export class AddeditassetComponent implements OnInit {

  constructor(private assetService: AssetService, private notificationService: NotificationService,
    public dialogRef: MatDialogRef<AddeditassetComponent>) { }

  ngOnInit() {
  }

  onSubmit() {

    if (this.assetService.form.valid) {

      if (this.assetService.form.value.AssetId === -1) {
        this.assetService.InsertAsset(this.assetService.form.value).subscribe(
          response => { this.onSuccess(); },
          error => {
            console.log(error);
            this.onError(error);
          }
        );
      } else {
        this.assetService.UpdateAsset(this.assetService.form.value).subscribe(
          response => { this.onSuccess(); },
          error => {
            console.log(error);
            this.onError(error);
          }
        );
      }

    }
  }

  onError(error): void {
    this.notificationService.warn(error.statusText);
  }




  onSuccess(): void {
    this.notificationService.success(':: Submitted successfully',true);
    this.InitializeForm();
    this.onClose();
    this.notificationService.loadData.next(true);
  }

  onClear() {
    this.InitializeForm();
  }

  onClose() {
    this.InitializeForm();
    this.dialogRef.close();
  }

  InitializeForm(): void {
    this.assetService.form.reset();
    this.assetService.initializeFormGroup();
  }


}
