import { Asset } from './asset';

export class PagedAsset {
    TotalAssets: number;
    Assets: Array<Asset>;
}
