export class Asset {
    AssetId: number;
    AssetGuid: string;
    FileName: string;
    MimeType: string;
    CreatedBy: string;
    Email: string;
    Country: string;
    Description: string;
}
