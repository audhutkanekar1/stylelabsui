import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Asset } from '../models/asset';
import { environment } from 'src/environments/environment';
import { PagedAsset } from '../models/pagedassets';
import { FormGroup, FormControl, Validators } from '@angular/forms';
//import * as _ from 'lodash';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class AssetService {

  constructor(private http: HttpClient) { }

  form: FormGroup = new FormGroup({
    AssetId: new FormControl(-1),
    AssetGuid: new FormControl('', Validators.required),
    FileName: new FormControl('', Validators.required),
    MimeType: new FormControl('', [Validators.required]),
    CreatedBy: new FormControl('', Validators.required),
    Email: new FormControl('', Validators.email),
    Country: new FormControl('', Validators.required),
    Description: new FormControl('')

  });


  initializeFormGroup() {
    this.form.setValue({
      AssetId: null,
      AssetGuid: this.newguidgen(),
      FileName: '',
      MimeType: '',
      CreatedBy: '',
      Email: '',
      Country: '',
      Description: ''
    });
  }

  private newguidgen() {
    function id() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return id() + id() + '-' + id() + '-' + id() + '-' + id() + '-' + id() + id() + id();
  }

  public GetPagedAssets(pageNumber: number, pageSize: number, fileName: string = ''): Observable<PagedAsset> {

    const endpointUrl = `${environment.AssetServiceURI}/GetPagedAssets?pageNumber=${pageNumber}&pageSize=${pageSize}&fileName=${fileName}`;
    const Asset$ = this.http.get<PagedAsset>(endpointUrl);
    return Asset$;
  }

  public InsertAsset(asset): Observable<{}> {
    const endpointUrl = `${environment.AssetServiceURI}/AddAsset`;
    return this.http.post(endpointUrl, asset, httpOptions);
  }

  public UpdateAsset(asset: Asset): Observable<{}> {

    const endpointUrl = `${environment.AssetServiceURI}/UpdateAsset`;
    return this.http.put<Asset>(endpointUrl, asset, httpOptions);
  }

  public DeleteAsset(assetId: number) {
    const endpointUrl = `${environment.AssetServiceURI}/DeleteAsset?assetId=${assetId}`;
    return this.http.delete(endpointUrl);

  }
  populateForm(asset) {
    this.form.setValue(asset);
  }

}
