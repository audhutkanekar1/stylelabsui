import { Component, OnInit, ViewChild } from '@angular/core';
import { Asset } from '../models/asset';
import { AssetService } from '../services/asset.service';
import { MatPaginator, MatTableDataSource, MatDialog, MatDialogConfig } from '@angular/material';
import { AddeditassetComponent } from '../addeditasset/addeditasset.component';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-assetlist',
  templateUrl: './assetlist.component.html',
  styleUrls: ['./assetlist.component.scss']
})
export class AssetlistComponent implements OnInit {

  displayedColumns: string[] = ['AssetGuid', 'FileName', 'MimeType', 'CreatedBy', 'Email', 'Country', 'actions'];
  dataSource: MatTableDataSource<Asset>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageIndex = 0;
  pageSize = 25;

  searchKey: string;

  constructor(private assetService: AssetService, private notificationService: NotificationService, private dialog: MatDialog) { }

  ngOnInit() {
    this.loadData(this.pageIndex, this.pageSize, this.searchKey);

    this.notificationService.loadData$.subscribe(pair => {
      this.loadData(this.pageIndex, this.pageSize, this.searchKey);
    });
  }

  loadData(pageNumber: number, pageSize: number, fileName: string = ''): void {
    this.assetService.GetPagedAssets(pageNumber, pageSize, fileName).subscribe(rslt => {
      console.log(rslt);
      if (rslt.TotalAssets > 0) {
        this.dataSource = new MatTableDataSource(rslt.Assets);
        this.paginator.length = rslt.TotalAssets;
      }
    });
  }

  public handlePage(e: any) {
    this.pageIndex = e.pageIndex;
    this.pageSize = e.pageSize;
    this.loadData(this.pageIndex, this.pageSize, this.searchKey);

  }

  onSearchClear() {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter() {
    this.loadData(0, 25, this.searchKey);
  }

  onCreate() {
    this.assetService.initializeFormGroup();
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    this.dialog.open(AddeditassetComponent, dialogConfig);
  }

  onEdit(row) {
    this.assetService.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    this.dialog.open(AddeditassetComponent, dialogConfig);
  }

  onDelete(AssetId) {
    if (confirm('Are you sure to delete this record ?')) {
      this.assetService.DeleteAsset(AssetId).subscribe(
        response => {
          this.notificationService.success(':: Deleted successfully', true);
        },
        error => {
          this.onError(error);
        }
      );
    }
  }

  onError(error): void {
    this.notificationService.warn(error.statusText);
  }

}
