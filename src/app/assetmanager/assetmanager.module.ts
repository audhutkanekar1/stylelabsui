import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetlistComponent } from './assetlist/assetlist.component';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material';
import { AddeditassetComponent } from './addeditasset/addeditasset.component';

@NgModule({
  declarations: [AssetlistComponent, AddeditassetComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatIconModule
  ]
})
export class AssetmanagerModule { }
