import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AssetlistComponent } from './assetmanager/assetlist/assetlist.component';
import {
  MatTableModule, MatIconModule, MatFormFieldModule,
  MatInputModule, MatListModule, MatProgressSpinnerModule,
  MatPaginatorModule, MatSortModule, MatDialogModule,
  MatToolbarModule, MatGridListModule, MatSnackBarModule
} from '@angular/material';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddeditassetComponent } from './assetmanager/addeditasset/addeditasset.component';

@NgModule({
  declarations: [
    AppComponent,
    AssetlistComponent,
    AddeditassetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatToolbarModule,
    MatGridListModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AddeditassetComponent]
})
export class AppModule { }
