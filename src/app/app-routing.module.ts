import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetlistComponent } from './assetmanager/assetlist/assetlist.component';

const routes: Routes = [
  { path: 'Assets', component: AssetlistComponent },
  { path: '', redirectTo: '/Assets', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
